import pandas as pd
import time
import argparse
import json
from os import walk, mkdir, path
from shutil import copyfile


def has_picture(entities):
    """
    Finds out if the tweet includes a Twitter image reference
    :param entities: Tweet attribute entities
    :return: True if there's an image, False if not
    """
    if 'media' in entities and len([x for x in entities['media'] if 'display_url' in x]):
        display_urls = [x['display_url'] for x in entities['media'] if 'display_url' in x]
        return len([x for x in display_urls if 'pic.twitter' in x]) >= 1
    else:
        return False


def main():
    parser = argparse.ArgumentParser(description="CATI Images and Tweets dataset filterer")
    parser.add_argument("-ip", "--input_tweet_path", dest="input_tweet_path", help="The path to the json twitter file")
    parser.add_argument("-ti", "--input_images_directory", dest="input_images_directory",
                        help="The path of the images directory")
    args = parser.parse_args()

    input_tweets_file_path = args.input_tweet_path
    input_image_directory_path = args.input_images_directory

    image_filenames = []

    for (dirpath, dirnames, filenames) in walk(input_image_directory_path):
        image_filenames.extend(filenames)

    print("Importing Tweets")
    data = pd.read_json(input_tweets_file_path)

    # Tweet preprocessing
    print("Preprocessing Tweets")
    start = time.time()
    data['id_str'] = data['id_str'].astype('str')
    tweet_ids = data['id_str'].to_list()
    data['has_picture'] = data['entities'].apply(has_picture)
    end = time.time()
    print("Successfully preprocessed tweets in ", end - start, ' seconds')

    # We exclude the images that don't belong to the collected tweets
    print("Excluding images not in tweets")
    start = time.time()
    image_ids = [(x.split('_')[0], x) for x in image_filenames if x.split('_')[0] in tweet_ids]

    # We export the filtered images into a new directory (same path with '_filtered' suffix)
    output_image_directory_path = input_image_directory_path + '_filtered'
    mkdir(output_image_directory_path)
    for image in image_ids:
        copyfile(path.join(input_image_directory_path, image[1]), path.join(output_image_directory_path, image[1]))

    end = time.time()

    print("Successfully excluded images in ", end - start, ' seconds')
    print("Images exported in ", output_image_directory_path)

    # We exclude tweets which have an image reference that couldn't be found in the image dataset
    print("Excluding Tweets with missing images")
    start = time.time()
    filtered_data = data[
        ((data['has_picture']) & (data['id_str'].isin([x[0] for x in image_ids]))) | (data['has_picture'] == False)]

    filtered_ids = filtered_data['id_str'].to_list()
    # We dump the filtered tweets into a new JSON file
    output_tweets_file_path = input_tweets_file_path.split('.json')[0] + '_image_filtered.json'

    count_added = 0
    count_rejected = 0

    with open(output_tweets_file_path, 'a') as myfile:

        with open(input_tweets_file_path) as f:
            for line in f:

                cleansed_line = line

                if line == '[\n':
                    myfile.write(cleansed_line)
                else:
                    if line.endswith('},\n'):
                        cleansed_line = line[:-2]
                    elif line.endswith(']'):
                        cleansed_line = line[:-1]

                    dict_line = json.loads(cleansed_line)
                    id_str = dict_line['id_str']

                    if id_str in filtered_ids:
                        myfile.write(line)
                        count_added+=1
                        print(id_str, " Conserved")
                    else:
                        count_rejected+=1
                        print(id_str, " Rejected")


    end = time.time()


    print("Successfully excluded tweets in ", end - start, ' seconds')
    print(count_added, ' tweets conserved')
    print(count_rejected, ' tweets rejected')
    print("Tweets exported in", output_tweets_file_path)


if __name__ == "__main__":
    main()
