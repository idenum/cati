from mabed.es_connector import Es_connector
from mabed.functions import Functions
#import argparser
#from  FullTextGetter import ArticleScrapperWithDownload

import time
import sys
import random
import json
import re
import requests
import unicodedata
import unidecode
from datetime import datetime, timezone

headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:74.0) Gecko/20100101 Firefox/74.0','X-JAVASCRIPT-ENABLED':'true','Accept-Encoding':'br, gzip, deflate','Referer':'www.google.fr','Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'}
from string import Template
from elasticsearch import Elasticsearch

import newspaper
from newspaper import Article, Config

# Instantiating the parser
#parser = argparse.ArgumentParser(description="CATI's Active Learning module")

#parser.add_argument("-i",
                    #"--index",
                    #dest="index",
                    #help="The target index to which to add the new field")


#args = parser.parse_args()

mapping = {"https://www.leprogres.fr/rss": "Le Progrès",
           "https://www.leprogres.fr/edition-lyon-villeurbanne/rss": "Le Progrès - Édition Lyon Villeurbanne",
           "https://tribunedelyon.fr/feed/": "Tribune de Lyon",
           "https://www.lyonplus.com/rss": "Lyon Plus",
           "https://www.lyoncapitale.fr/rss": "Lyon Capitale",
           "https://www.lyonmag.com/rss": "Lyon Mag",
           "https://www.leprogres.fr/edition-est-lyonnais/rss" : "Le Progrès - Édition Est Lyonnais",
           "https://www.leprogres.fr/edition-ouest-lyonnais/rss": "Le Progrès - Édition Ouest Lyonnais",
           "https://www.leprogres.fr/edition-sud-lyonnais/rss": "Le Progrès - Édition Sud Lyonnais"}

index = "lyon_journaux"

def update_articles(docs, connector, langs=["en", "fr", "es"]):
    for article in docs:

        full_text = "<a href='{}'> <b> {} </b> </a>".format(article["_source"]["link"], article["_source"]["title"]) + "<br />" + "<br />" + article["_source"]["message"]
        connector.es.update(
            index=index,
            id=article["_id"],
            body={"doc": {
                "full_text": full_text
            }})
        date_parse = datetime.strptime(article["_source"]["published"],"%Y-%m-%dT%H:%M:%S.%fZ")
        tuple = date_parse.timetuple()
        stamp = int(time.mktime(tuple))*1000
        date_parse = datetime.strftime(date_parse,'%a %b %d %H:%M:%S +0000 %Y')
        connector.es.update(
            index=index,
            id=article["_id"],
            body={"doc": {
                "created_at": date_parse
            }})
        connector.es.update(
            index=index,
            id=article["_id"],
            body={"doc": {
                "timestamp_ms": stamp
            }})
        connector.es.update(
            index=index,
            id=article["_id"],
            body={"doc": {
                "user":{"name": mapping[article["_source"]["Feed"]]}
            }})

#        try1:
#            article_scrapped = ArticleScrapperWithDownload(article["_source"]["link"])
#            text = article_scrapped.preprocessAndExtraction()
#            text = unicodedata.normalize('NFKD', text).encode('ascii','ignore')
#            text = text.decode("utf-8")
#
#            connector.es.update(
#                index=index,
#                id=article["_id"],
#                body={"doc": {
 #               "body": text
#                }})

#        except:
#            print("Fail")


my_connector = Es_connector(index=index)
query = {
    "query": {
        "match_all": {}
    }
}
# query = {
#     "query": {
#         "match": {
#             "lang": "en or fr or es"
#         }
#     }
# }
res = my_connector.init_paginatedSearch(query=query)

sid = res["sid"]
scroll_size = res["scroll_size"]
init_total = int(res["total"])
accum_total = 0

print("\nTotal = ", init_total)
print("\nScroll = ", scroll_size)



while scroll_size > 0:

    update_articles(res["results"], my_connector, ["fr"])
    res = my_connector.loop_paginatedSearch(sid, scroll_size)
    scroll_size = res["scroll_size"]
    accum_total += scroll_size
    print(accum_total*100/init_total, "%")

