import json
import ast
import plotly.plotly as py
import plotly.graph_objs as go
import plotly.io as pio
import os
import re
import statistics
import operator
import numpy as np
import math
import matplotlib.pyplot as plt

def read_file(path):
    file = open(path, "r")
    logs = '['
    for line in file:

        # This is fixing the bug for the first printed results

        line = line.replace('", "f1"', ', "f1"')
        line = line.replace('", "recall"', ', "recall"')
        line = line.replace('", "precision"', ', "precision"')
        line = line.replace('", "positive_precision"', ', "positive_precision"')
        line = line.replace('", "wrong_pred_answers"', ', "wrong_pred_answers"')

        logs = logs + line

    logs = logs[:-1]
    logs = logs + ']'
    textual_logs = logs.replace('\n', ',')

    return json.loads(textual_logs)

def process_results(logs):
    loop_logs = [log for log in logs if 'loop' in log]

    loops_values = [log["loop"] for log in logs if 'loop' in log]  # datetime
    accuracies = [log["accuracy"] for log in logs if 'loop' in log]
    # diff_accuracies = [float(log["diff_accuracy"]) for log in logs if 'loop' in log if log["diff_accuracy"] != 'None']
    wrong_answers = [log["wrong_pred_answers"] for log in logs if 'loop' in log]

    return loops_values, accuracies, wrong_answers


# Initialization
# logs_folders = [f.path for f in os.scandir(logs_path) if f.is_dir() ]
configs = []
loop_prefix = "loop "


def get_config_value(prop_name, full_text):
    start_index = len(prop_name)
    end_index = start_index + 3
    name = full_text[start_index:end_index]
    return name

def get_value_at_loop(prop_name, loop_index, logs):

    target_loop = [log for log in logs if log["loop"] == loop_index]

    return target_loop[0]

def get_config_name(full_text):

    hyp = re.search(r'HYP', full_text, re.M | re.I)
    if hyp is None:
        return get_config_value("best_config_", full_text)
    else:
        return "HYP"

def print_measurements(measurements, title, limit):
    print("\n", title)
    for idx, measure in enumerate(measurements):
        if idx < limit:
            print(idx, measure)


full_scenario_results = []
top_accuracy_scenario_results = []
top_precision_scenario_results = []
meta_measurements = []

scenarios_paths = "C:\\Users\\gbosetti\\Dropbox\\event tracking\\journal_experiments"
#scenario_path = "C:\\Users\\gbosetti\\Dropbox\\event tracking\\journal_experiments\\session_IMG_2016_FOOT\\raw-results"
scenarios_paths = [os.path.join(f.path, "raw-results") for f in os.scandir(scenarios_paths) if f.is_dir()]
measurements = []
target_samplers = ["UncertaintySampler", "RandomSampler"]
alpha = 0.2
beta = 0.9
num_of_datasets = 4

for scenario_path in scenarios_paths:

    # Get all the OUR files for the session
    config_files = [f for f in os.scandir(scenario_path) if not f.is_dir()]

    # GET THE AVERAGES AND STDEVS (MEASUREMENTS) FOR EACH CONFIGURATION
    for config_file in config_files:

        if config_file.name != 'download.txt':
            # For each configuration
            # Get the logs of the only file for HYP
            config = read_file(config_file.path)
            config_name = get_config_name(config_file.name)
            loops = [line for line in config if 'loop' in line]
            sampler = [line for line in config if 'sampler' in line][0]['sampler']

            if sampler not in target_samplers:
                continue

            # ACCURACY
            accuracies = np.array([log["accuracy"] for log in loops], dtype=float)
            gradients = np.gradient(accuracies)
            sum_areas = np.sum(accuracies)
            count_neg_gradients = np.sum([abs(grad) for grad in gradients if grad < 0])  # len([grad for grad in gradients if grad < 0])
            count_all_gradients = np.sum([abs(grad) for grad in gradients])

            ac_1 = alpha * (sum_areas/len(accuracies))
            ac_2 = (1-alpha)*(1-(count_neg_gradients/count_all_gradients))
            accuracy_score = ac_1 + ac_2

            # PRECISION
            field = np.array([log["precision"] for log in loops], dtype=float)
            gradients = np.gradient(field)
            sum_areas = np.sum(field)
            count_neg_gradients = len([grad for grad in gradients if grad < 0])
            count_all_gradients = len(gradients)
            precision_score = sum_areas/count_all_gradients + (1 - (count_neg_gradients / count_all_gradients))

            # wrong_pred_answers (MAY BE DIFFERENT)
            field = np.array([log["wrong_pred_answers"] for log in loops], dtype=float)
            gradients = np.gradient(field)
            sum_areas = np.sum(field)
            count_pos_gradients = len([grad for grad in gradients if grad >= 0])
            count_all_gradients = len(gradients)
            corrective_clicks_score = sum_areas/count_all_gradients + (1 - (count_pos_gradients / count_all_gradients))

            #accuracy_average = statistics.mean(accuracies)
            #accuracy_stdev = statistics.stdev(accuracies)
            # precisions = [log["precision"] for log in loops]
            # precisErythromelalgia ion_average = statistics.mean(precisions)
            # precision_stdev = statistics.stdev(precisions)
            # recalls = [log["recall"] for log in loops]
            # recall_average = statistics.mean(recalls)
            # recall_stdev = statistics.stdev(recalls)
            clicks = [log["wrong_pred_answers"] for log in loops]
            clicks_average = statistics.mean(clicks)
            # clicks_stdev = statistics.stdev(clicks)

            matching_measures = [msr for msr in measurements if msr['name'] == config_name]

            if len(matching_measures)==0:
                measurements.append({
                    "name": config_name,
                    "sampler": sampler,
                    "learner": [line for line in config if 'learner' in line][0],
                    "vectorizer": [line for line in config if 'vectorizer' in line][0],
                    "field": [line for line in config if 'field' in line][0],
                    "count_neg_gradients": count_neg_gradients,
                    "count_all_gradients": count_all_gradients,
                    "accuracy_score": accuracy_score,
                    "precision_score": precision_score,
                    "corrective_clicks_score": corrective_clicks_score,
                    "clicks_average": clicks_average,
                    "accuracies": [accuracies],
                    "loops": [log["loop"] for log in loops]
                })
            else:
                matching_measures[0]["accuracy_score"] += accuracy_score
                matching_measures[0]["precision_score"] += precision_score
                matching_measures[0]["corrective_clicks_score"] += corrective_clicks_score
                #matching_measures[0]["accuracy_average"] += accuracy_average
                # matching_measures[0]["precision_average"] += precision_average
                # matching_measures[0]["recall_average"] += recall_average
                matching_measures[0]["clicks_average"] += clicks_average
                matching_measures[0]["accuracies"].append(accuracies)


# SORT THE CONFIGS BY HIGER AVERAGE AND LOWER STDEV
# measurements.sort(key=lambda i: (i['accuracy_average']), reverse=True) # , -i['accuracy_stdev']
# print_measurements(measurements, "Top accuracies")
# measurements.sort(key=lambda i: (i['precision_average']), reverse=True)
# print_measurements(measurements, "Top precisions")
# measurements.sort(key=lambda i: (i['recall_average']), reverse=True)
# print_measurements(measurements, "Top recall")
# measurements.sort(key=lambda i: (-i['clicks_average'], i['apr'], -i['apr_stdev']), reverse=True)
# print_measurements(measurements, "Both criteria", 10)
#
# measurements.sort(key=lambda i: (i['apr'], -i['apr_stdev']), reverse=True) # , -i['accuracy_stdev']
# print_measurements(measurements, "Top APR", 10)
# measurements.sort(key=lambda i: (-i['clicks_average']), reverse=True)
# print_measurements(measurements, "Top lower corrective-clicks", 10)

measurements.sort(key=lambda i: (i['accuracy_score']), reverse=True)
print_measurements([m["name"] for m in measurements], "Best for ACCURACY", 10)
# print_measurements([m["name"] for m in measurements], "Best for ACCURACY", 5)

# measurements.sort(key=lambda i: (i['precision_score']), reverse=True)
# print_measurements(measurements, "Best for PRECISION", 10)
#
#measurements.sort(key=lambda i: (-i['clicks_average']), reverse=True)
#print_measurements(measurements, "Best for AVERAGE CORRECTIVE CLICKS (divide it by num of datasets)", 10)






def KL(a, b):
    a = np.asarray(a, dtype=np.float)
    b = np.asarray(b, dtype=np.float)

    return np.sum(np.where(a != 0, a * np.log(a / b), 0))

measures_names = list(set([msr['name'] for msr in measurements]))
measures_names.sort()

unified_measurements = []
for measures_name in measures_names:

    matching_measures = [msr for msr in measurements if msr['name'] == measures_name]
    accum_divergence = 0
    total_divs = 0
    avg_accuracy_score=0
    max_divergence = 0

    for m_measure in matching_measures:
        for i in range(0, len(m_measure["accuracies"])-1):
            accum_divergence += abs(KL(m_measure["accuracies"][i], m_measure["accuracies"][i+1]))
            total_divs += 1
            #else: print("Skipping")
            if accum_divergence > max_divergence:
                max_divergence = accum_divergence

        avg_accuracy_score += m_measure["accuracy_score"]

        full_accuracies = [item for sublist in m_measure["accuracies"] for item in sublist]
        accuracy_average_across_datasets = np.sum(full_accuracies)/len(full_accuracies)

        unified_measurements.append({
            "name": m_measure["name"],
            "total_divs": total_divs,
            "accum_divergence": accum_divergence,
            "avg_accuracy_score": avg_accuracy_score/num_of_datasets,
            "accuracy_score": avg_accuracy_score,  # matching_measures["accuracy_score"]
            "accuracies": m_measure["accuracies"],
            "accuracy_average_across_datasets": accuracy_average_across_datasets,
            "loops": m_measure["loops"],
            "clicks_average": m_measure["clicks_average"]
        })

max_divergence = max([div["accum_divergence"] for div in unified_measurements])
for measure in unified_measurements:
    print(measure["name"], measure["total_divs"], max_divergence)
    measure["divergence_average"] = (measure["accum_divergence"] / measure["total_divs"]) / max_divergence
    measure["score"] = beta * measure["avg_accuracy_score"] + (1-beta) * (1-measure["divergence_average"])

# unified_measurements.sort(key=lambda i: (-i['divergence_average']), reverse=True)
# print_measurements(unified_measurements, "divergence_average", 5)

unified_measurements.sort(key=lambda i: (i['score']), reverse=True)
print_measurements(unified_measurements, "score", 5)






















# Functions
class MultiCOnfigSubplots():

    def __init__(self, configs=[], ncols=3, subplot_width=6, subplot_height=3, wspace=0.35, hspace=1.75):
        self.ncols = ncols
        self.subplot_width = subplot_width
        self.subplot_height = subplot_height
        self.wspace = wspace
        self.hspace = hspace
        self.configs = configs

    def read_file(self, path):
        file = open(path, "r")
        logs = '['
        for line in file:
            # This is fixing the bug for the first printed results
            line = line.replace('", "f1"', ', "f1"')
            line = line.replace('", "recall"', ', "recall"')
            line = line.replace('", "precision"', ', "precision"')
            line = line.replace('", "positive_precision"', ', "positive_precision"')
            line = line.replace('", "wrong_pred_answers"', ', "wrong_pred_answers"')
            line = line.replace(',,', ',')
            logs = logs + line

        logs = logs[:-1]
        logs = logs + ']'
        textual_logs = logs.replace('\n', ',')

        return json.loads(textual_logs)

    def get_formatted_logs(self, json_content):
        logs = [line for line in json_content if 'loop' in line]

        return {
            "loops": [line["loop"] for line in logs],
            "accuracies": [line["accuracy"] for line in logs],
            "precisions": [line["precision"] for line in logs],
            "f1": [line["f1"] for line in logs],
            "recalls": [line["recall"] for line in logs],
            "wrong_pred_answers": [line["wrong_pred_answers"] for line in logs]
        }

    def get_graph_title_from_logs(self, json_content, file_index=""):

        # field = [line["field"] for line in json_content if 'field' in line][0]
        # vectorizer = [line["vectorizer"] for line in json_content if 'vectorizer' in line][0]
        # learner = [line["learner"] for line in json_content if 'learner' in line][0]
        # sampler = [line["sampler"] for line in json_content if 'sampler' in line][0]
        # return str(file_index) + "\n·" + vectorizer + " (field: " + field + ")\n·" + learner + "\n·" + sampler + "\n"  # str(file_index + 1) + "\n

        return "Configuration #" + json_content["name"] + " (score: " + str(round(json_content["score"], 3)) + ")"

    def generate_graphs(self, input_folder, output_filename, target_fields):

        for config in self.configs:

            nrows = math.ceil(len(self.configs) / self.ncols)

            fig, axs = plt.subplots(nrows=nrows, ncols=self.ncols, figsize=(self.ncols*self.subplot_width, self.subplot_height*nrows))  # constrained_layout=True)
            flatten_axes = enumerate(fig.axes)

            for file_index, cog in enumerate(self.configs):
                filename = cog["name"]
                accuracies = cog["accuracies"]
                axis = flatten_axes.__next__()[1]

                # Drawing the required lines (one per field) in a same subplot
                # if (1 + file_index) % self.ncols == 0:
                #     show_label = True
                # else: show_label = False
                show_label = False

                for acc in cog["accuracies"]:
                    for field_index, target_field in enumerate(target_fields):

                        color = target_field["color"]
                        if show_label:
                            axis.plot(cog["loops"], acc, marker='o', markersize=3, color=color, markerfacecolor=color, label=target_field["display"])
                            axis.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
                        else: axis.plot(cog["loops"], acc, marker='o', markersize=3, color=color, markerfacecolor=color)

                        # Adding the numbers on the points
                        deplacement = 0
                        for i, txt in enumerate(acc):
                            axis.annotate(round(txt, 3),
                                          (cog["loops"][i] + deplacement, acc[i] + deplacement))

                    # Setting the subplot properties
                    subplot_pos = axis.get_position()
                    axis.set_title(self.get_graph_title_from_logs(cog), loc='left')
                    axis.set(xlabel="loops")  #, ylabel=ylabel)
                    axis.xaxis.set_ticks(cog["loops"])
                    plt.subplots_adjust(hspace=self.hspace, wspace=self.wspace)
                    plt.close(fig)

        plt.subplots_adjust(hspace=self.hspace, wspace=self.wspace)
        fig.savefig(output_filename, transparent=False, bbox_inches='tight')

# -------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------

plotter = MultiCOnfigSubplots(configs=unified_measurements, ncols=2, subplot_width=5, subplot_height=3, wspace=0.35, hspace=1)
input_folder = os.path.join(os.getcwd(), "classification", "logs")

plotter.generate_graphs(input_folder, output_filename="accuracy_final.png", target_fields=[
    {"name":"accuracies", "display":"Accuracy", "color": "blue"}
])
print("Done")



