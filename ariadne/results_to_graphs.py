import json
import ast
import plotly.plotly as py
import plotly.graph_objs as go
import plotly.io as pio
import os
import re
import numpy as np
import matplotlib.pyplot as plt
import math

# Functions
class MultiCOnfigSubplots():

    def __init__(self, ncols=3, subplot_width=6, subplot_height=3, wspace=0.35, hspace=1.75):
        self.ncols = ncols
        self.subplot_width = subplot_width
        self.subplot_height = subplot_height
        self.wspace = wspace
        self.hspace = hspace

    def read_file(self, path):
        file = open(path, "r")
        logs = '['
        for line in file:
            # This is fixing the bug for the first printed results
            line = line.replace('", "f1"', ', "f1"')
            line = line.replace('", "recall"', ', "recall"')
            line = line.replace('", "precision"', ', "precision"')
            line = line.replace('", "positive_precision"', ', "positive_precision"')
            line = line.replace('", "wrong_pred_answers"', ', "wrong_pred_answers"')
            line = line.replace(',,', ',')
            logs = logs + line

        logs = logs[:-1]
        logs = logs + ']'
        textual_logs = logs.replace('\n', ',')

        return json.loads(textual_logs)

    def get_formatted_logs(self, json_content):
        logs = [line for line in json_content if 'loop' in line]

        return {
            "loops": [line["loop"] for line in logs],
            "accuracies": [line["accuracy"] for line in logs],
            "precisions": [line["precision"] for line in logs],
            "f1": [line["f1"] for line in logs],
            "recalls": [line["recall"] for line in logs],
            "wrong_pred_answers": [line["wrong_pred_answers"] for line in logs]
        }

    def get_graph_title_from_logs(self, json_content, file_index=""):

        field = [line["field"] for line in json_content if 'field' in line][0]
        vectorizer = [line["vectorizer"] for line in json_content if 'vectorizer' in line][0]
        learner = [line["learner"] for line in json_content if 'learner' in line][0]
        sampler = [line["sampler"] for line in json_content if 'sampler' in line][0]
        return str(file_index) + "\n·" + vectorizer + " (field: " + field + ")\n·" + learner + "\n·" + sampler + "\n"  # str(file_index + 1) + "\n

    def generate_graphs(self, input_folder, output_filename, target_fields):

        log_files = [f.path for f in os.scandir(input_folder) if
                     "best_config_" in f.path] # Reading all the files
        nrows = math.ceil(len(log_files) / self.ncols)

        fig, axs = plt.subplots(nrows=nrows, ncols=self.ncols, figsize=(self.ncols*self.subplot_width, self.subplot_height*nrows))  # constrained_layout=True)
        flatten_axes = enumerate(fig.axes)

        for file_index, path in enumerate(log_files):
            filename = os.path.basename(path)[-7:-4]
            json_content = self.read_file(path)
            logs = self.get_formatted_logs(json_content)
            axis = flatten_axes.__next__()[1]

            # Drawing the required lines (one per field) in a same subplot
            if (1 + file_index) % self.ncols == 0:
                show_label = True
            else: show_label = False

            for field_index, target_field in enumerate(target_fields):

                color = target_field["color"]

                if show_label:
                    axis.plot(logs["loops"], logs[target_field["name"]], marker='o', markersize=3, color=color, markerfacecolor=color, label=target_field["display"])
                    axis.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
                else: axis.plot(logs["loops"], logs[target_field["name"]], marker='o', markersize=3, color=color, markerfacecolor=color)

                # Adding the numbers on the points
                deplacement = 0
                for i, txt in enumerate(logs[target_field["name"]]):
                    axis.annotate(round(txt, 3),
                                  (logs["loops"][i] + deplacement, logs[target_field["name"]][i] + deplacement))

            # Setting the subplot properties
            subplot_pos = axis.get_position()
            #fig.suptitle(file_index, fontsize=18, x=subplot_pos.x0-1, y=subplot_pos.y0-1)
            axis.set_title(self.get_graph_title_from_logs(json_content,filename), loc='left')
            axis.set(xlabel="loops")  #, ylabel=ylabel)
            axis.xaxis.set_ticks(logs["loops"])

        plt.subplots_adjust(hspace=self.hspace, wspace=self.wspace)
        fig.savefig(output_filename, transparent=False, bbox_inches='tight')

# -------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------

plotter = MultiCOnfigSubplots(ncols=3, subplot_width=5, subplot_height=3, wspace=0.35, hspace=1.75)
input_folder = os.path.join(os.getcwd(), "classification", "logs")

plotter.generate_graphs(input_folder, output_filename="accuracy.png", target_fields=[
    {"name":"accuracies", "display":"Accuracy", "color": "blue"}
])
plotter.generate_graphs(input_folder, output_filename="precision_recall.png", target_fields=[
    {"name":"precisions", "display":"Precision", "color": "orange"},
    {"name": "recalls", "display": "Recall", "color": "green"}
])
plotter.generate_graphs(input_folder, output_filename="corrective_clicks.png", target_fields=[
    {"name":"wrong_pred_answers", "display":"Corrective clicks", "color": "red"}
])
