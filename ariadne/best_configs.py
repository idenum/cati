import json
import ast
import plotly.plotly as py
import plotly.graph_objs as go
import plotly.io as pio
import os
import re
import statistics
import operator

def read_file(path):
    file = open(path, "r")
    logs = '['
    for line in file:

        # This is fixing the bug for the first printed results
        line = line.replace('", "f1"', ', "f1"')
        line = line.replace('", "recall"', ', "recall"')
        line = line.replace('", "precision"', ', "precision"')
        line = line.replace('", "positive_precision"', ', "positive_precision"')
        line = line.replace('", "wrong_pred_answers"', ', "wrong_pred_answers"')

        logs = logs + line

    logs = logs[:-1]
    logs = logs + ']'
    textual_logs = logs.replace('\n', ',')

    return json.loads(textual_logs)

def process_results(logs):
    loop_logs = [log for log in logs if 'loop' in log]

    loops_values = [log["loop"] for log in logs if 'loop' in log]  # datetime
    accuracies = [log["accuracy"] for log in logs if 'loop' in log]
    # diff_accuracies = [float(log["diff_accuracy"]) for log in logs if 'loop' in log if log["diff_accuracy"] != 'None']
    wrong_answers = [log["wrong_pred_answers"] for log in logs if 'loop' in log]

    return loops_values, accuracies, wrong_answers


# Initialization
# logs_folders = [f.path for f in os.scandir(logs_path) if f.is_dir() ]
configs = []
loop_prefix = "loop "


def get_config_value(prop_name, full_text):
    start_index = len(prop_name)
    end_index = start_index + 3
    name = full_text[start_index:end_index]
    return name

def get_value_at_loop(prop_name, loop_index, logs):

    target_loop = [log for log in logs if log["loop"] == loop_index]

    return target_loop[0]

def get_config_name(full_text):

    hyp = re.search(r'HYP', full_text, re.M | re.I)
    if hyp is None:
        return get_config_value("best_config_", full_text)
    else:
        return "HYP"

def print_measurements(measurements, title, limit):
    print("\n", title)
    for idx, measure in enumerate(measurements):
        if idx < limit:
            print(idx, measure)


full_scenario_results = []
top_accuracy_scenario_results = []
top_precision_scenario_results = []
meta_measurements = []

scenarios_paths = "C:\\Users\\gbosetti\\Dropbox\\event tracking\\journal_experiments"
#scenario_path = "C:\\Users\\gbosetti\\Dropbox\\event tracking\\journal_experiments\\session_IMG_2016_FOOT\\raw-results"
scenarios_paths = [os.path.join(f.path, "raw-results") for f in os.scandir(scenarios_paths) if f.is_dir()]
measurements = []
target_samplers = ["UncertaintySampler"]  #"RandomSampler",

for scenario_path in scenarios_paths:

    # Get all the OUR files for the session
    config_files = [f for f in os.scandir(scenario_path) if not f.is_dir()]

    # GET THE AVERAGES AND STDEVS (MEASUREMENTS) FOR EACH CONFIGURATION
    for config_file in config_files:

        if config_file.name != 'download.txt':
            # For each configuration
            # Get the logs of the only file for HYP
            config = read_file(config_file.path)

            config_name = get_config_name(config_file.name)
            loops = [line for line in config if 'loop' in line]
            sampler = [line for line in config if 'sampler' in line][0]['sampler']

            if sampler not in target_samplers:
                continue

            accuracies = [log["accuracy"] for log in loops]
            accuracy_average = statistics.mean(accuracies)
            accuracy_stdev = statistics.stdev(accuracies)

            precisions = [log["precision"] for log in loops]
            precision_average = statistics.mean(precisions)
            precision_stdev = statistics.stdev(precisions)

            recalls = [log["recall"] for log in loops]
            recall_average = statistics.mean(recalls)
            recall_stdev = statistics.stdev(recalls)

            clicks = [log["wrong_pred_answers"] for log in loops]
            clicks_average = statistics.mean(clicks)
            clicks_stdev = statistics.stdev(clicks)

            matching_measures = [msr for msr in measurements if msr['name'] == config_name]

            if len(matching_measures)==0:
                measurements.append({
                    "name": config_name,
                    "sampler": sampler,
                    "learner": [line for line in config if 'learner' in line][0],
                    "vectorizer": [line for line in config if 'vectorizer' in line][0],
                    "field": [line for line in config if 'field' in line][0],
                    "accuracy_average": accuracy_average,
                    "precision_average": precision_average,
                    "recall_average": recall_average,
                    "clicks_average": clicks_average,
                    "clicks_stdev": clicks_stdev,
                    "apr": accuracy_average + precision_average + recall_average,
                    "apr_stdev": accuracy_stdev + precision_stdev + recall_stdev
                })
            else:
                matching_measures[0]["accuracy_average"] += accuracy_average
                matching_measures[0]["precision_average"] += precision_average
                matching_measures[0]["recall_average"] += recall_average
                matching_measures[0]["clicks_average"] += clicks_average


# SORT THE CONFIGS BY HIGER AVERAGE AND LOWER STDEV
# measurements.sort(key=lambda i: (i['accuracy_average']), reverse=True) # , -i['accuracy_stdev']
# print_measurements(measurements, "Top accuracies")
# measurements.sort(key=lambda i: (i['precision_average']), reverse=True)
# print_measurements(measurements, "Top precisions")
# measurements.sort(key=lambda i: (i['recall_average']), reverse=True)
# print_measurements(measurements, "Top recall")

measurements.sort(key=lambda i: (i['apr'], -i['apr_stdev']), reverse=True) # , -i['accuracy_stdev']
print_measurements(measurements, "Top APR", 10)

measurements.sort(key=lambda i: (-i['clicks_average']), reverse=True)
print_measurements(measurements, "Top lower corrective-clicks", 10)

# measurements.sort(key=lambda i: (-i['clicks_average'], i['apr'], -i['apr_stdev']), reverse=True)
# print_measurements(measurements, "Both criteria", 10)




