import json
import ast
import os
import re
import statistics
import operator

def read_file(path):
    file = open(path, "r")
    logs = '['
    for line in file:

        # This is fixing the bug for the first printed results
        line = line.replace('", "f1"', ', "f1"')
        line = line.replace('", "recall"', ', "recall"')
        line = line.replace('", "precision"', ', "precision"')
        line = line.replace('", "positive_precision"', ', "positive_precision"')
        line = line.replace('", "wrong_pred_answers"', ', "wrong_pred_answers"')

        logs = logs + line

    logs = logs[:-1]
    logs = logs + ']'
    textual_logs = logs.replace('\n', ',')

    return json.loads(textual_logs)


def accum_measures(config_0, config_files_1):

    config_0_sampler = [line for line in config_0 if 'sampler' in line][0]["sampler"]
    config_0_learner = [line for line in config_0 if 'learner' in line][0]["learner"]
    config_0_vectorizer = [line for line in config_0 if 'vectorizer' in line][0]["vectorizer"]
    config_0_field = [line for line in config_0 if 'field' in line][0]["field"]
    config_0_loops = [line for line in config_0 if 'loop' in line]

    for file_1 in config_files_1:

        config_1 = read_file(file_1.path)
        config_1_sampler = [line for line in config_1 if 'sampler' in line][0]["sampler"]
        config_1_learner = [line for line in config_1 if 'learner' in line][0]["learner"]
        config_1_vectorizer = [line for line in config_1 if 'vectorizer' in line][0]["vectorizer"]
        config_1_field = [line for line in config_1 if 'field' in line][0]["field"]
        config_1_loops = [line for line in config_1 if 'loop' in line]

        if config_0_sampler == config_1_sampler and \
                config_0_learner == config_1_learner and \
                config_0_vectorizer == config_1_vectorizer and \
                config_0_field == config_1_field:

            for i, loop_0 in enumerate(config_0_loops):
                config_0_loops[i]["accuracy"] += config_1_loops[i]["accuracy"]
                config_0_loops[i]["f1"] += config_1_loops[i]["f1"]
                config_0_loops[i]["recall"] += config_1_loops[i]["recall"]
                config_0_loops[i]["precision"] += config_1_loops[i]["precision"]
                config_0_loops[i]["wrong_pred_answers"] += config_1_loops[i]["wrong_pred_answers"]

            break

def divide_measures(config, div_times):

    loops = [line for line in config if 'loop' in line]

    for loop in loops:
        loop["accuracy"] = loop["accuracy"] / div_times
        loop["f1"] = loop["f1"] / div_times
        loop["recall"] = loop["recall"] / div_times
        loop["precision"] = loop["precision"] / div_times
        loop["wrong_pred_answers"] = loop["wrong_pred_answers"] / div_times


config_files_0 = [f for f in os.scandir("C:\\Users\\gbosetti\\Dropbox\\event tracking\\journal_experiments\\lyon_tweets\\logs-random-0") if not f.is_dir()]
config_files_1 = [f for f in os.scandir("C:\\Users\\gbosetti\\Dropbox\\event tracking\\journal_experiments\\lyon_tweets\\logs-random-1") if not f.is_dir()]
config_files_2 = [f for f in os.scandir("C:\\Users\\gbosetti\\Dropbox\\event tracking\\journal_experiments\\lyon_tweets\\logs-random-2") if not f.is_dir()]
output_dir = "C:\\Users\\gbosetti\\Dropbox\\event tracking\\journal_experiments\\lyon_tweets\\logs-random-average\\"

for file_0 in config_files_0:

    config_0 = read_file(file_0.path)
    accum_measures(config_0, config_files_1)
    accum_measures(config_0, config_files_2)
    divide_measures(config_0, 3)

    with open(output_dir + file_0.name, 'w') as f:
        content = json.dumps(config_0)
        content = content[1:-1]
        content = content.split("},")

        for i, line in enumerate(content):
            # line = line.replace(', "f1"', '", "f1"')
            # line = line.replace(', "recall"', '", "recall"')
            # line = line.replace(', "precision"', '", "precision"')
            # line = line.replace(', "positive_precision"', '", "positive_precision"')
            # line = line.replace(', "wrong_pred_answers"', '", "wrong_pred_answers"')
            if i < len(content)-1:
                f.write(line.strip() + '}\n')

        print(content)

