import json
import ast
import plotly.plotly as py
import plotly.graph_objs as go
import plotly.io as pio
import os
import re
import statistics
import operator
import numpy as np
import matplotlib.pyplot as plt
import math

def read_file(path):
    file = open(path, "r")
    logs = '['
    for line in file:

        # This is fixing the bug for the first printed results

        line = line.replace('", "f1"', ', "f1"')
        line = line.replace('", "recall"', ', "recall"')
        line = line.replace('", "precision"', ', "precision"')
        line = line.replace('", "positive_precision"', ', "positive_precision"')
        line = line.replace('", "wrong_pred_answers"', ', "wrong_pred_answers"')

        logs = logs + line

    logs = logs[:-1]
    logs = logs + ']'
    textual_logs = logs.replace('\n', ',')

    return json.loads(textual_logs)

def process_results(logs):
    loop_logs = [log for log in logs if 'loop' in log]

    loops_values = [log["loop"] for log in logs if 'loop' in log]  # datetime
    accuracies = [log["accuracy"] for log in logs if 'loop' in log]
    # diff_accuracies = [float(log["diff_accuracy"]) for log in logs if 'loop' in log if log["diff_accuracy"] != 'None']
    wrong_answers = [log["wrong_pred_answers"] for log in logs if 'loop' in log]

    return loops_values, accuracies, wrong_answers


# Initialization
# logs_folders = [f.path for f in os.scandir(logs_path) if f.is_dir() ]
configs = []
loop_prefix = "loop "


def get_config_value(prop_name, full_text):
    start_index = len(prop_name)
    end_index = start_index + 3
    name = full_text[start_index:end_index]
    return name

def get_value_at_loop(prop_name, loop_index, logs):

    target_loop = [log for log in logs if log["loop"] == loop_index]

    return target_loop[0]

def get_config_name(full_text):

    hyp = re.search(r'HYP', full_text, re.M | re.I)
    if hyp is None:
        return get_config_value("best_config_", full_text)
    else:
        return "HYP"

def print_measurements(measurements, title, limit):
    print("\n", title)
    for idx, measure in enumerate(measurements):
        if idx < limit:
            print(idx, measure)


full_scenario_results = []
top_accuracy_scenario_results = []
top_precision_scenario_results = []
meta_measurements = []

scenarios_paths = "C:\\Users\\gbosetti\\Dropbox\\event tracking\\journal_experiments"
#scenario_path = "C:\\Users\\gbosetti\\Dropbox\\event tracking\\journal_experiments\\session_IMG_2016_FOOT\\raw-results"
scenarios_paths = [os.path.join(f.path, "raw-results") for f in os.scandir(scenarios_paths) if f.is_dir()]
measurements = []
target_samplers = ["UncertaintySampler", "RandomSampler"]

for scenario_path in scenarios_paths:

    # Get all the OUR files for the session
    config_files = [f for f in os.scandir(scenario_path) if not f.is_dir()]
    scenario_name = scenario_path.split("\\")[-2]
    # GET THE AVERAGES AND STDEVS (MEASUREMENTS) FOR EACH CONFIGURATION
    for config_file in config_files:

        if config_file.name != 'download.txt':
            # For each configuration
            # Get the logs of the only file for HYP
            config = read_file(config_file.path)
            config_name = get_config_name(config_file.name)
            loops = [line for line in config if 'loop' in line]
            sampler = [line for line in config if 'sampler' in line][0]['sampler']

            if sampler not in target_samplers:
                continue

            # ACCURACY
            accuracies = [log["accuracy"] for log in loops]
            field = np.array(accuracies, dtype=float)
            gradients = np.gradient(field)
            sum_areas = np.sum(field)
            count_neg_gradients = len([grad for grad in gradients if grad < 0])
            count_all_gradients = len(gradients)
            accuracy_score = sum_areas/count_all_gradients + (1-(count_neg_gradients/count_all_gradients))

            # PRECISION
            field = np.array([log["precision"] for log in loops], dtype=float)
            gradients = np.gradient(field)
            sum_areas = np.sum(field)
            count_neg_gradients = len([grad for grad in gradients if grad < 0])
            count_all_gradients = len(gradients)
            precision_score = sum_areas/count_all_gradients + (1 - (count_neg_gradients / count_all_gradients))

            # wrong_pred_answers (MAY BE DIFFERENT)
            field = np.array([log["wrong_pred_answers"] for log in loops], dtype=float)
            gradients = np.gradient(field)
            sum_areas = np.sum(field)
            count_pos_gradients = len([grad for grad in gradients if grad >= 0])
            count_all_gradients = len(gradients)
            corrective_clicks_score = sum_areas/count_all_gradients + (1 - (count_pos_gradients / count_all_gradients))
            clicks = [log["wrong_pred_answers"] for log in loops]
            clicks_average = statistics.mean(clicks)
            #matching_measures = [msr for msr in measurements if msr['name'] == config_name]

            measurements.append({
                "name": config_name,
                "scenario_name": scenario_name,
                "sampler": sampler,
                "learner": [line for line in config if 'learner' in line][0],
                "vectorizer": [line for line in config if 'vectorizer' in line][0],
                "field": [line for line in config if 'field' in line][0],
                "count_neg_gradients": count_neg_gradients,
                "count_all_gradients": count_all_gradients,
                "accuracies": accuracies,
                "loops": [log["loop"] for log in loops],
                "accuracy_score": accuracy_score,
                "precision_score": precision_score,
                "corrective_clicks_score": corrective_clicks_score,
                "clicks_average": clicks_average
            })

def KL(a, b):
    a = np.asarray(a, dtype=np.float)
    b = np.asarray(b, dtype=np.float)

    return np.sum(np.where(a != 0, a * np.log(a / b), 0))

measures_names = list(set([msr['name'] for msr in measurements]))
measures_names.sort()

unified_measurements = []
for measures_name in measures_names:

    matching_measures = [msr for msr in measurements if msr['name'] == measures_name]

    accum_divergence = 0
    total_divs = 0
    for m_measure_1 in matching_measures:
        for m_measure_2 in matching_measures:

            if m_measure_1 != m_measure_2:
                accum_divergence += KL(m_measure_1["accuracies"], m_measure_2["accuracies"])
                total_divs += 1
                #else: print("Skipping")

    unified_measurements.append({
        "name": m_measure_1["name"],
        "divergence_average": accum_divergence / total_divs
    })




unified_measurements.sort(key=lambda i: (-i['divergence_average']), reverse=True)
print_measurements(unified_measurements, "divergence_average", 10)

# generate_graphs(unified_measurements, "paper_figure.png", [
#     {"name":"accuracies", "display":"Accuracy", "color": "blue"}
# ], 3, 6, 3, 0.35, 1.75)

print("end")
#
# measurements.sort(key=lambda i: (i['precision_score']), reverse=True)
# print_measurements(measurements, "Best for PRECISION", 10)
#
# measurements.sort(key=lambda i: (-i['clicks_average']), reverse=True)
# print_measurements(measurements, "Best for AVERAGE CORRECTIVE CLICKS", 10)