import pandas as pd
from classification.active_learning import ActiveLearning
from fast_bert import BertLMDataBunch, BertLMLearner, BertDataBunch, BertLearner
from fast_bert.prediction import BertClassificationPredictor
from fast_bert.metrics import accuracy
from pathlib import Path
import logging
import torch
import os
torch.cuda.empty_cache()


logger = logging.getLogger()
device_cuda = torch.device("cuda")
metrics = []
al_classifier = ActiveLearning(download_folder_name="tmp_data")


DATA_PATH = Path('camembert/data/')
LOG_PATH = Path('camembert/logs/')
MODEL_PATH = Path('camembert/model/')
LABEL_PATH = Path('camembert/labels/')

OUTPUT_DIR = Path('camembert/finetuned_model')
WGTS_PATH = Path('camembert/model/model_out/pytorch_model.bin')

data_train, data_test, data_unlabeled, categories = al_classifier.loading_tweets_from_files()

df_train = pd.DataFrame()
df_train['text'] = data_train['data']
df_train['target']=data_train['target']
df_train['positive'] = df_train['target'].astype(int)
df_train['negative'] = 1 - df_train['target'].astype(int)

df_train = df_train.drop(columns=['target'])


df_test = pd.DataFrame()

df_test['text'] = data_test['data']
df_test['target'] = data_test['target']
df_test['positive'] = df_test['target'].astype(int)
df_test['negative'] = 1 - df_test['target'].astype(int)

df_test = df_test.drop(columns=['target'])


df_train.to_csv('camembert/data/train_set.csv')
df_test.to_csv('camembert/data/val_set.csv')

labels = df_train.columns[1:].to_list()
with open ('camembert/labels/labels.txt', 'w') as f:
    for i in labels:
        f.write(i + "\n")


all_texts = df_train['text'].to_list() + df_test['text'].to_list()

databunch_lm = BertLMDataBunch.from_raw_corpus(data_dir=DATA_PATH,
                                               text_list=all_texts,
                                               tokenizer='camembert-base',
                                               batch_size_per_gpu=2,
                                               max_seq_length=512,
                                               multi_gpu=False,
                                               logger=logger,
                                               model_type='camembert-base')


lm_learner = BertLMLearner.from_pretrained_model(dataBunch=databunch_lm,
                                                 pretrained_path='camembert-base',
                                                 output_dir=MODEL_PATH,
                                                 metrics=[],
                                                 multi_gpu=False,
                                                 logger=logger,
                                                 device=device_cuda,
                                                 fp16_opt_level="O2")



lm_learner.fit(epochs=30,
               lr=1e-6,
               validate=True,
               schedule_type="warmup_cosine",
               optimizer_type="adamw")

lm_learner.validate()
lm_learner.save_model(Path('camembert/model/model_out'))

databunch = BertDataBunch(DATA_PATH, LABEL_PATH,
                          tokenizer='camembert-base',
                          train_file='train_set.csv',
                          val_file='val_set.csv',
                          label_file='labels.txt',
                          text_col='text',
                          label_col=['positive', 'negative'],
                          batch_size_per_gpu=4,
                          max_seq_length=512,
                          multi_gpu=False,
                          multi_label=True,
                          model_type='camembert-base')

learner = BertLearner.from_pretrained_model(
                        databunch,
                        pretrained_path='camembert/model/model_out',
                        metrics=metrics,
                        device=device_cuda,
                        logger=logger,
                        output_dir=OUTPUT_DIR,
                        finetuned_wgts_path=WGTS_PATH,
                        warmup_steps=300,
                        multi_gpu=True,
                        is_fp16=True,
                        multi_label=False,
                        logging_steps=50)

# learner.validate()
learner.save_model(Path('camembert/finetuned_model/model_out'))


predictor = BertClassificationPredictor(model_path ='camembert/finetuned_model/model_out',
                                    label_path='camembert/labels/',
                                    multi_label=True,
                                    model_type='camembert-base',
                                    do_lower_case=False)


