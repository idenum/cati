import json
import re
import argparse
import os

from mabed.es_connector import Es_connector
from tqdm import tqdm
from detectionyolov9 import annotate, load_model


__author__ = "Firas Odeh"
__email__ = "odehfiras@gmail.com"

if __name__ == '__main__':

    os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

    p = argparse.ArgumentParser(description='Import images clusters into Elasticsearch index')

    p.add_argument('-i', metavar='index', type=str, help='The Elasticsearch index you want to associate the image clusters')
    p.add_argument('-d', metavar='index', type=str, help='The image directory')
    p.add_argument('-u', metavar='index', type=bool, help='Tag images only on existing clusters')


    args = p.parse_args()

    with open('config.json', 'r') as f:
        config = json.load(f)

    for source in config['elastic_search_sources']:
        if source['index'] == args.i:
            filename = source['image_duplicates']

    print('File: %s\nIndex: %s\n' % (filename, args.i))

    with open(filename) as f:
        data = json.load(f)

    print('Number of clusters: %d' % len(data['duplicates']))
    print('Index', args.i)

    my_connector = Es_connector(index=args.i)
    imgs = 0
    count = 0
    c_count = 0

    res = my_connector.search({

        "size": "0",
        "aggs": {
            "uniq_tags": {
                "terms": {"field": "imagesCluster",

                "size": 100000
            }
            }
        }
    })

    if args.u==True:
        keys = [x['key'] for x in
                res['aggregations']['uniq_tags']['buckets']]

        data['duplicates'] = [data['duplicates'][x] for x in keys]

    model = load_model('yolov3.h5')
    for cluster in tqdm(data['duplicates']):

        img = cluster[0]
        image_file_name = re.search(r'(?<=/)(\d*)_(.*)\.(.*)', img, re.M | re.I).group(0)


#        annotation = annotate(model, os.path.join(args.d, image_file_name))

        for img in cluster:
            imgs += 1
            print("     Image ", imgs)
            target_tweet_id = os.path.basename(img).split("_")[0]

            # other_target_tweet_id  = re.search(r'(?<=/)(\d*)_(.*)\.(.*)', img, re.M | re.I)
            # other = other_target_tweet_id.group(1)

            res = my_connector.search({
                "query": {
                    "term": {"id_str": target_tweet_id}  # target_tweet_id.group(1)}
                }})
            if res['hits']['total']['value'] > 0:

                id = res['hits']['hits'][0]['_id']
                if 'imagesCluster' in res['hits']['hits'][0]['_source']:
                    arr = res['hits']['hits'][0]['_source']['imagesCluster']
                    if isinstance(arr, list):
                        arr.extend([c_count])
                        arr = list(set(arr))
                        update = my_connector.update_field(id, 'imagesCluster', arr)
                    else:
                        update = my_connector.update_field(id, 'imagesCluster', [arr])
                else:
                    update = my_connector.update_field(id, 'imagesCluster', [c_count])


 #               update = my_connector.update_field(id, 'imagesAnnotation', [annotation])

            count += res['hits']['total']['value']
        c_count += 1
        print('CLuster ', c_count, ' -----------------------')
    print('images %d' % imgs)
    print('count %d' % count)
