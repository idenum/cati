class TagCloudModule extends SearchModule{

    constructor(containerSelector,client ) {

        super(containerSelector);
        this.client = client;
    }

    loadTweets(data){
        new SearchModule(".tagcloud_container", "spinner-individual").enableLoading();
        var tagCloudSelector = '.tagcloud_container';
        this.client.showLoadingMessage(tagCloudSelector, 200);

        this.requestTagCloud(data).then(response => {
            $(tagCloudSelector).html('<div id="tagcloud" style="height: 300px; width: 200px%; margin-left:auto; margin-right:auto;"></div>')
            new SearchModule(".tagcloud_container", "spinner-individual").disableLoading();

            this.createTagCloud(response, data, data[1].value, data[2].value);
        });


    }

    requestTagCloud(data) {

        return new Promise((resolve, reject)=>{
            $.post(app.appURL+'get_tag_cloud', data, function(response){

                 resolve(response);

            }, 'json').fail(function(err) { console.log("Error:", err); });
        });
    }

    createTagCloud(response, data, start_date, end_date){

      this.presentTagCloud(response, data, start_date, end_date, this.containerSelector, this.client);

    }

    presentTagCloud(words, data, start_date, end_date, selector, client){
    anychart.onDocumentReady(function () {


        // create a tag cloud chart
        var chart = anychart.tagCloud(words);


        // set the chart title
        chart.title('Most significantly associated terms')
        // set array of angles, by which words will be placed
        chart.angles([0])
        chart.normal().fontWeight(600);
        chart.mode("spiral");
        chart.listen("pointClick", function (e) {
            console.log(e.point.get("x"));
            var new_data = data.slice();
            new_data[1].value = start_date;
            new_data[2].value = end_date;
            new_data.push({name: "tag_word", value: e.point.get("x")});

            $.confirm({
                theme: 'pix-cluster-modal',
                title: 'Detailed tweets',
                columnClass: 'col-md-12',
                useBootstrap: true,
                backgroundDismiss: false,
                content: 'Loading... <div class=" jconfirm-box jconfirm-hilight-shake jconfirm-type-default  jconfirm-type-animated loading" role="dialog"></div>',
                defaultButtons: false,
                onContentReady: function () {
                    var jc = this;


                    $.post(app.appURL + 'get_tag_tweets', new_data, function (response) {
                        console.log(new_data)
                        var html = client.get_retweets_html(response.duplicates.aggregations.top_text.buckets, 'static_tweet_box') + client.get_tweets_html(response.single, '');
                        client.delegateEvents();
                        jc.setContent(html);
                    }, 'json').fail(function () {
                        $.confirm({
                            title: 'Error',
                            boxWidth: '600px',
                            theme: 'pix-danger-modal',
                            backgroundDismiss: true,
                            content: "An error was encountered while connecting to the server, please try again.<br>Error code: tweets__user_tweets",
                            buttons: {
                                cancel: {
                                    text: 'CLOSE',
                                    btnClass: 'btn-cancel',
                                }
                            }
                        });
                    });
                }
            });

        });
                    // display chart
            chart.container(selector);
            chart.draw();
    })}

}
